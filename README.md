# lvr_dbs

Project to import and sync lv-r.de dbms to and from internal dbms

## Environment

You`ll need a .env file in the root of the app Folder:

```
.
├── .env
├── Pipfile
├── Pipfile.lock
├── README.md
├── checking_dbs.py
├── main.py
├── requirements.txt
├── snippets
├── syncdbms
└── todo.md
```

```
# API lvr
API_LVR_TOKEN="..."

# POSTGRES
DB_PSQL_HOST="..."
DB_PSQL_USER="..."
DB_PSQL_PASSWD="..."
DB_PSQL_DB="..."

# MSSQL
DB_MSSQL_HOST="..."
DB_MSSQL_PORT="..."
DB_MSSQL_USER="..."
DB_MSSQL_PASSWD="..."

# SYNCING
WRITE_BACK_DB="..."
CIRCLE="..."
TIMEOUT_SYNC=...

```