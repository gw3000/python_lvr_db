"""main module to manage all data exchange from and to platform database
and in house web app database in a given time windows"""
from os import environ as e
from pathlib import Path
from time import sleep, time

import dotenv
import schedule

from syncdbms.db_worker import (address_worker, agent_worker, contract_worker,
                                status_worker)
from syncdbms.logger import logging
from syncdbms.logger import seperator as sep
from syncdbms.ram_worker import (address_worker_ramicro,
                                 contract_worker_ramicro, rsv_worker_ramicro)

dotenv.load()
env_file = Path(".env")
time_out = int(e.get("TIMEOUT_SYNC"))


def import_worker():
    """a worker function the manages all data stuff"""
    # begin of event™
    time_begin = time()
    sep()
    logging.info("DB-WORKER: started syncing!")
    sep()
    agent_worker.agent_worker()
    sep()
    address_worker.address_worker()
    sep()
    contract_worker.contracts_worker()
    sep()
    status_worker.status_worker()
    sep()
    address_worker_ramicro.sync_adresses()
    sep()
    contract_worker_ramicro.contract_worker_ramicro()
    sep()
    rsv_worker_ramicro.rsv_worker_ramicro()
    sep()
    # end of event
    time_end = time()

    logging.info(
        f"DB-WORKER: Done after {round(time_end - time_begin, 2)} seconds!")
    sep()


schedule.every(time_out).minutes.do(import_worker)

if __name__ == "__main__":
    # import_worker()
    if env_file.is_file():
        if e.get("CIRCLE") == "true":
            while True:
                try:
                    schedule.run_pending()
                except ConnectionError as error:
                    logging.error(f"Connection-Error: {error}")
                except BaseException as error:
                    logging.error(error)
                else:
                    sleep(time_out)
        else:
            import_worker()
    else:
        print(".env does not exist")
