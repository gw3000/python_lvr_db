"""module to sync contracts between lvr-db and ramicro"""
from datetime import date

from syncdbms.logger import logging
from syncdbms.sql_worker import postgres
from syncdbms.sql_worker.mssql import mssql

today = "CONVERT(datetime,'" + date.today().strftime("%d.%m.%Y") + "', 104)"

# eAkte Unterordner (IDNodeText)
node_text = [38, 54, 55, 56]


def check_akz_in_ram():
    """check if contracts are in RAMICRO. If not check 'in_ram'
    to postgres database"""

    # select contracts which are not in ram from postgres
    res_list = ""
    sql = (
        f"{'SELECT refnum FROM t_contracts '}"
        f"{'WHERE in_ram is false or in_ram is null;'}"
    )
    result = postgres(sql, fetch=True)
    for item in result:
        res_list += "'" + item[0] + "',"

    #  now check RAMIRCO wich contract exists
    if res_list:
        sql = (
            f"SELECT sAktenNummer FROM tblAkten "
            f"WHERE sAktenNummer IN ({res_list[:-1]});"
        )
        res_ms = mssql("RAMICRO", sql, fetch=True)

    # set in_ram = true in postgres
    sql = ""
    count = 0
    if res_ms:
        for akz in res_ms:
            sql += (
                f"UPDATE t_contracts SET in_ram = TRUE "
                f" WHERE refnum='{akz[0]}';")
            count += 1
        postgres(sql)

    logging.info(f"SQL: set 'IN RAM' for {count} contracts")


def contract_worker_ramicro():
    """worker to add records to RAMICRO"""
    check_akz_in_ram()


if __name__ == "__main__":
    check_akz_in_ram()
