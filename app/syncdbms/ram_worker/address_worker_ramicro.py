"""module to sync the addresses between lvr app and ramicro"""
from time import time

from syncdbms.helpers import db_null, gender_greeting, salutation
from syncdbms.logger import logging
from syncdbms.sql_worker.mssql import mssql
from syncdbms.sql_worker.postgres import postgres


def get_address_lvr():
    """check wich address isn't in mssql"""
    sql = "SELECT * FROM t_address;"
    return postgres(sql, fetch=True)


def check_max_muster():
    """check if id is in ram with name 'Max Mustermann'"""
    ram_ids = ""
    pg_address = get_address_lvr()
    for row in pg_address:
        ram_ids += str(row[0]) + ","

    if ram_ids:
        sql = (
            f"SELECT iAdressNummer,sNachname,sVorname,sStraße,sOrt,sPLZ,"
            f"sErsteAdressZeile,sTelefon,sMobiltelefon,sEMail "
            f"FROM tblAdressen "
            f"WHERE iAdressNummer IN ({ram_ids[:-1]})"
            f"ORDER BY iAdressnummer;"
        )
        res_ram = mssql("RAMICRO", sql, fetch=True)

        addr_for_pg = []  # addresses from ramicro to postgres
        addr_for_ram = []  # addresses from postgres to ramicro
        ids_for_pg = []

        for item in res_ram:
            if item[1] == "Mustermann" and item[2] == "Max":
                ids_for_pg.append(item[0])
            else:
                addr_for_pg.append(item)

        for addr in pg_address:
            for id in ids_for_pg:
                if addr[0] == id:
                    addr_for_ram.append(addr)

        # Addresses sent to lvr Postgres
        write_address_to_lvr(addr_for_pg)

        # Addresses sent to RAMICRO MSSQL
        write_address_to_ramicro(addr_for_ram)

        # set in_ram in lvr Postgres
        write_in_ram(ram_ids)


def write_address_to_ramicro(addr_list):
    """write address to ramicro"""
    sql = ""
    for row in addr_list:
        sql += (
            f"UPDATE RAMICRO.dbo.tblAdressen SET "
            f"sTitel={db_null(row[7])},"
            f"sAnrede={str(gender_greeting(row[8])[0])},"
            f"sBriefanrede={salutation(row[8], row[1])},"
            f"sErsteAdresszeile={str(gender_greeting(row[8])[1])},"
            f"sVorname={db_null(row[2])},"
            f"sNachname={db_null(row[1])},"
            f"sStraße={db_null(row[3])},"
            f"sPLZ={db_null(row[5])},"
            f"sOrt={db_null(row[4])},"
            f"sTelefon={db_null(row[9])},"
            f"sMobiltelefon={db_null(row[10])},"
            f"sEMail={db_null(row[11])} "
            f"WHERE iAdressNummer={str(row[0])};"
        )

    if sql != "":
        mssql("RAMICRO", sql)
        logging.info(f"{'MSSQL: Wrote addresses to RAM'}")
    else:
        logging.info(f"{'MSSQL: No address was written to RAM'}")


def write_address_to_lvr(addr_list):
    """write address to postgres lvr"""
    sql = ""
    for row in addr_list:
        sql += (
            f"UPDATE t_address SET "
            f"lname={db_null(row[1])}, fname={db_null(row[2])}, "
            f"street={db_null(row[3])},city={db_null(row[4])}, "
            f"zip={db_null(row[5])}, salutation={db_null(row[6])}, "
            f"phone={db_null(row[7])},mobile={db_null(row[8])}, "
            f"mail={db_null(row[9])} "
            f"WHERE id={db_null(row[0])};"
        )

    if sql != "":
        postgres(sql)
        logging.info(f"{'SQL: Updated Adresses from RAM to postgres'}")
    else:
        logging.info(f"{'SQL: No address was written to postgres'}")


def write_in_ram(ids):
    """set bit "in_ram" to true (1) to db_lvr"""
    sql = f"UPDATE t_address SET in_ram = TRUE WHERE id IN ({ids[:-1]});"
    postgres(sql)
    logging.info(f"{'SQL: Wrote IN RAM to postgres'}")


def sync_adresses():
    """a function to work them all"""
    time_begin = time()
    check_max_muster()

    #  write_address_ramicro()
    #  write_in_ram()
    #  update_address_in_postgres()

    time_end = time()
    logging.info(f"MSSQL: Done after {round(time_end - time_begin, 2)} seconds!")


if __name__ == "__main__":
    sync_adresses()
