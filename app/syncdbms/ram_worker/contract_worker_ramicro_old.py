import sql_dbv
import sql_ram
import sql_ram_earchiv
import logging
import modules as m
from datetime import date

today = date.today().strftime("%d.%m.%Y")
today = "CONVERT(datetime,'" + today + "', 104)"

# eAkte Unterordner (IDNodeText)
node_text = [38, 54, 55, 56]


def sync_contracts():
    # change log-level and config of logging"
    logging.basicConfig(
        filename='logs/sync_contracts.log',
        level=logging.INFO,
        format='%(asctime)s:%(levelname)s:%(message)s:')

    # query to ask dbv for contracts and akz
    query_dbv = "SELECT co.id_mbd, concat(co.akz_c, '/' , akz_y) as sAktenNummer, concat(ad.name, ', ', ad.vorname, ' ./. ', ge.vs_darstellg) as abz,  CURRENT_TIMESTAMP() AS dtAnlage, '20' as iReferat, co.land as sAktenKennzeichen, '1' as aab, ge.vs_auswahl as bemerkung, '0' as  gegenstandswert, '1899-12-30 00:00:00' as dtAblage, '0' as  iAblageNummer, '1' as yAktenArt, (co.akz_y+2000)as iAktenjahrgang, (ad.id+40000) as iLaufendeNummer, '0' as yIDUnterakte,  concat(ad.name, ', ', ad.vorname) as sMandant, 'gw' as  sDiktatzeichenErsteller, akz_c, akz_y, ad.id+40000, ge.anum_ram,  co.betnum FROM t_contracts co INNER JOIN  t_address ad ON  co.id_mbd=ad.id INNER JOIN t_gesellschaft ge ON  co.idt_gesellschaft=ge.idt_gesellschaft WHERE co.in_ram IS NULL AND  id_mbd IS NOT NULL AND co.idt_gesellschaft > 0  AND ge.anum_ram IS NOT NULL AND ad.vorname IS NOT NULL  ORDER BY id_mbd ASC;"

    result_dbv = sql_dbv.read(query_dbv)

    if len(result_dbv) > 0:
        for r in result_dbv:
            if len(r[2]) > 25:
                akb = r[2][:25]  # sAktenKurzBezeichnung
                ab = r[2][25:]  # sAktenBezeichnung
                land = m.country_code(r[5])  # Länderkürzel

                query_ram = "INSERT INTO RAMICRO.DBO.TBLAKTEN (sAktenNummer,sAktenKurzBezeichnung,sAktenBezeichnung,dtAnlage,iReferat,sAktenSachbearbeiter,sAktenKennzeichen,yAnzahlAuftraggeber,sBemerkung,cGegenstandswert,dtAblage,iAblageNummer,iLaufendeNummer,iAktenjahrgang, yIDUnterAkte, sMandant, iFarbeAktennotiz, yAktenArt) VALUES ('{}', '{}', '{}', {}, '{}', '{}', '{}', '{}', '{}', '{}',{},'{}',{},{},{},'{}',{},{}) ;". format(
                    r[1], akb, ab, today, r[4], 'FS', land, r[6], r[7][:63], '0', "CONVERT(datetime, '31.12.1899', 104)", '0', r[17], r[12], '0', r[15], '0', '1')
                sql_ram.write(query_ram)
                # print(query_ram + "\n")

# ------------------------------
    # drop the temporary table
    query = "IF EXISTS (SELECT * FROM sys.tables WHERE name = 'tt_sync_db' AND schema_id = schema_id('dbo')) BEGIN DROP TABLE RAMICRO.dbo.tt_sync_db END;"
    sql_ram.write(query)

    # create the table
    query = "CREATE TABLE RAMICRO.dbo.tt_sync_db (sAktenNummer VARCHAR(20),GUIDAdresse VARCHAR(100),GUIDAkte VARCHAR(100),iAdressNummer INT,GUIDAdresse_Gegner VARCHAR(100),iAdressNummer_Gegner INT, sVertrag VARCHAR(100));"
    sql_ram.write(query)

    # init sync table in ram
    query = "TRUNCATE TABLE RAMICRO.dbo.tt_sync_db;"
    sql_ram.write(query)

    # insert akz into tt_sync_db
    for item in result_dbv:
        query_tt_sync_db_ram = "INSERT INTO tt_sync_db (sAktenNummer, iAdressNummer, iAdressNummer_Gegner, sVertrag) VALUES ('{}', '{}', '{}', '{}');".format(
            item[1], item[-3], item[-2], item[-1])
        sql_ram.write(query_tt_sync_db_ram)

    # merging GUIDAkte into tt_sync_db
    query = "MERGE INTO tt_sync_db T USING tblAkten S ON T.sAktenNummer = S.sAktenNummer WHEN MATCHED THEN UPDATE SET GUIDAkte = S.GUIDAkte;"
    sql_ram.write(query)

    # merging GUIAdresse into tt_sync_db
    query = "MERGE INTO tt_sync_db T USING tblAdressen S ON T.iAdressNummer = S.iAdressNummer WHEN MATCHED THEN UPDATE SET T.GUIDAdresse = S.GUIDAdresse;"
    sql_ram.write(query)

    # merging GUIDAdresse Gegner into tt_sync_db
    query = "MERGE INTO tt_sync_db T USING tblAdressen S ON T.iAdressNummer_Gegner = S.iAdressNummer WHEN MATCHED THEN UPDATE SET T.GUIDAdresse_Gegner = S.GUIDAdresse;"
    sql_ram.write(query)
# ------------------------------
    # aktenbeteiligte
    query = "SELECT sAktenNummer, GUIDAdresse, GUIDAkte, iAdressNummer, GUIDAdresse_Gegner, iAdressNummer_Gegner, sVertrag FROM RAMICRO.dbo.tt_sync_db WHERE GUIDAkte IS NOT NULL;"
    result_ram_ab = sql_ram.read(query)

    for r in result_ram_ab:
        # beteiligter zu 1
        query_ab_01 = "INSERT INTO RAMICRO.dbo.tblAktenBeteiligte (GUIDAdresse, GUIDAkte, iIDBeteiligter, iBeteiligtenArt, sBeteiligtenKennzeichen, sBetreffZeile1, sBetreffZeile2, sBetreffZeile3, iIDAnsprechPartner, bVorsteuerabzug, bDeaktiviert, iRangfolge, iUnterbeteiligte, iAdressnummer, cSelbstbeteiligung) VALUES('{}', '{}', 0, 1, '', 'Rückabwicklung Versicherungsvertrag', 'Versicherungsnummer: {}', '', 0, 0, 0, 1, 0, {}, 0);".format(
            r[1], r[2], r[-1], r[3])
        sql_ram.write(query_ab_01)
        # print(query_ab_01)

        # beteiligter zu 2
        query_ab_02 = "INSERT INTO RAMICRO.dbo.tblAktenBeteiligte (GUIDAdresse, GUIDAkte, iIDBeteiligter, iBeteiligtenArt, sBeteiligtenKennzeichen, sBetreffZeile1, sBetreffZeile2, sBetreffZeile3, iIDAnsprechPartner, bVorsteuerabzug, bDeaktiviert, iRangfolge, iUnterbeteiligte, iAdressnummer, cSelbstbeteiligung) VALUES('{}', '{}', 1, 2, '', 'Versicherungsnummer: {}', '', '', 0, 0, 0, 1, 0, {}, 0);".format(
            r[4], r[2], r[-1], r[5])
        sql_ram.write(query_ab_02)
        # print(query_ab_02)

        # eAkte
        rea = r[0].split('/')
        for i in node_text:
            query_ram_eakte = "INSERT INTO raEloakte.dbo.tblElo_TreeNodes (IDParentNode, IDNodeText) VALUES ('A\\\\20{}\\{}\\0', {});".format(
                rea[1], rea[0], i)
            sql_ram_earchiv.write(query_ram_eakte)
            print(query_ram_eakte)

        # dbv update
        query_contracts = "UPDATE t_contracts SET in_ram = 1 WHERE betnum = '{}';".format(
            r[-1])
        sql_dbv.write(query_contracts)
        # print(query_contracts + "\n")

        # write to logfile
        ins_log = ('ADDED BETNUM IN RAMICRO.dbo.tblAkten: {}'.format(r[-1]))
        logging.info(ins_log)


# the function alone
sync_contracts()
print('Ende!')
# closing the mysql-connection
sql_dbv.connection.close()
# closing the mysql-connection
sql_ram.connection.close()
