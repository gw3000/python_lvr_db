"""address worker to fetch addresses from old lvr database to new one"""
import dotenv
from sql_worker import sql_worker_mysql as mysql
from sql_worker import sql_worker as postgres

dotenv.load()


# +++++++++++++ functions +++++++++++++

def apo(item):
    return "'" + item + "'"


def value_none(item):
    """returns NULL if value was None"""
    item = str(item)
    if item != "None":
        val = "'" + item + "'"
    else:
        val = "NULL"
    return val


def person(item):
    """return specific wording on item input"""
    if item in ("Familie", "Herrn und Frau", "Eheleute"):
        val = "'Eheleute/Lebenspartnerschaft'"
    elif item in ("Frau", "Herrn"):
        val = "'eine Einzelperson'"
    return val


def service_provider(item):
    """returns numbers of certain service providers"""
    if item == "Hof":
        val = 13  # andreas nierlich
    elif item == "IVGEN":
        val = 4  # serdar ivgen
    else:
        val = "NULL"

    return val


def ccc(item):
    """counntry of conclusion of contract return true or false"""
    if item == "GER":
        val = "true"
    elif item == "A":
        val = "false"
    else:
        val = "NULL"
    return val


def cont_type(vorsortierung, vertragsart):
    """return sorting and content type input"""
    if vorsortierung == "Riester":
        val = "'Riesterrente'"
    elif vorsortierung == "Rürup":
        val = "'Basis-/Rüruprente'"
    elif vertragsart == "Rentenversicherung":
        val = "'sonstiger Renten- oder Lebensversicherungsvertrag'"
    elif vertragsart == "Lebensversicherung":
        val = "'sonstiger Renten- oder Lebensversicherungsvertrag'"
    else:
        val = "NULL"
    return val


def cont_status(presort, finish):
    """returns contract status"""
    if presort == "laufende Verträge":
        val = "'laufender Vertrag'"
    elif finish == 1:
        val = "'beendeter Vertrag (gekündigt oder ausgelaufen)'"
    else:
        val = "NULL"
    return val


def adding_block(item):
    """returns zusatzversicherung"""
    if item == "ohne Zusatzbaustein":
        val = "1"
    else:
        val = "NULL"
    return val


def abtretung(item):
    """returns true if item is 1"""
    if item == 1:
        val = "true"
    else:
        val = "false"
    return val


def insuranced_person(item):
    """returns null, true or false on given insuranced person"""
    if item == 0:
        val = "NULL"
    elif item == 1:
        val = "true"
    elif item == 2:
        val = "false"
    return val


def gender_insuranced_person(item):
    """returns the gender of a person"""
    if item == 1:
        val = "'weiblich'"
    elif item == 2:
        val = "'männlich'"
    else:
        val = "NULL"
    return val


def follow(item):
    if item == ("Erbschein", "Erbvertrag", "Testament", "keine Unterlagen"):
        val = "true"
    else:
        val = "NULL"
    return val


def mandatsstatus(item):
    """takes old ms id and returns new one"""
    try:
        return {
            "None": "Null",
            5: 8030,
            16: 5150,
            21: 8090,
            65: 7160,
            70: 6160,
            71: 5250,
            73: 5270,
            99: 5230,
            125: 9010,
        }[item]
    except KeyError:
        return item


def rsv(item):
    """takes old rsv id returns new one"""
    try:
        return {"None": "NULL", 2: 8, 5: 7}[item]
    except KeyError:
        return item


def company(id):
    """takes old company id and returns new company id"""
    try:
        return {
            "None": "NULL",
            238: 1,
            1: 2,
            237: 3,
            239: 4,
            2: 5,
            303: 6,
            240: 7,
            3: 8,
            4: 9,
            5: 10,
            6: 11,
            7: 12,
            315: 13,
            360: 14,
            8: 15,
            9: 16,
            241: 17,
            312: 18,
            10: 19,
            11: 20,
            12: 21,
            356: 22,
            308: 23,
            13: 24,
            354: 25,
            320: 26,
            14: 27,
            15: 28,
            16: 29,
            563: 30,
            17: 31,
            18: 32,
            620: 33,
            19: 34,
            20: 35,
            21: 36,
            612: 37,
            22: 38,
            645: 39,
            647: 40,
            512: 41,
            508: 42,
            23: 43,
            24: 44,
            25: 45,
            26: 46,
            28: 47,
            29: 48,
            27: 49,
            31: 50,
            30: 51,
            347: 52,
            242: 53,
            32: 54,
            33: 55,
            34: 56,
            622: 57,
            349: 58,
            506: 59,
            505: 60,
            644: 61,
            35: 62,
            36: 63,
            37: 65,
            38: 66,
            244: 67,
            39: 68,
            243: 64,
            629: 69,
            40: 70,
            41: 71,
            335: 72,
            334: 73,
            43: 75,
            42: 74,
            44: 76,
            615: 77,
            45: 78,
            245: 79,
            246: 80,
            281: 81,
            247: 82,
            46: 83,
            47: 84,
            48: 85,
            49: 86,
            543: 87,
            50: 88,
            51: 89,
            52: 90,
            54: 91,
            53: 92,
            55: 93,
            248: 94,
            250: 95,
            56: 96,
            249: 97,
            57: 98,
            58: 99,
            59: 100,
            60: 101,
            574: 102,
            576: 103,
            573: 104,
            283: 105,
            62: 106,
            61: 107,
            63: 108,
            346: 109,
            251: 110,
            64: 111,
            65: 112,
            66: 113,
            67: 114,
            68: 115,
            297: 116,
            307: 117,
            69: 118,
            631: 119,
            70: 120,
            71: 121,
            72: 122,
            73: 123,
            74: 124,
            75: 125,
            76: 126,
            77: 127,
            623: 128,
            78: 129,
            338: 130,
            337: 131,
            336: 132,
            79: 133,
            82: 134,
            83: 135,
            353: 136,
            355: 137,
            352: 138,
            656: 139,
            80: 140,
            660: 141,
            81: 142,
            84: 143,
            638: 144,
            85: 145,
            639: 146,
            627: 147,
            294: 148,
            86: 149,
            301: 150,
            87: 151,
            88: 152,
            319: 153,
            252: 154,
            253: 155,
            285: 156,
            513: 157,
            511: 158,
            91: 159,
            92: 160,
            350: 161,
            351: 162,
            93: 163,
            99: 164,
            100: 165,
            101: 166,
            102: 167,
            254: 168,
            103: 169,
            255: 170,
            106: 171,
            564: 172,
            104: 173,
            105: 174,
            305: 175,
            108: 177,
            107: 176,
            256: 178,
            109: 179,
            257: 180,
            110: 181,
            651: 182,
            652: 183,
            111: 185,
            613: 184,
            616: 186,
            614: 187,
            339: 188,
            112: 189,
            113: 190,
            114: 191,
            119: 192,
            611: 193,
            115: 194,
            116: 195,
            117: 196,
            624: 197,
            118: 198,
            566: 199,
            610: 200,
            120: 201,
            121: 202,
            122: 203,
            641: 204,
            630: 205,
            640: 206,
            571: 207,
            359: 208,
            123: 209,
            124: 210,
            125: 211,
            128: 212,
            129: 213,
            309: 214,
            310: 215,
            534: 216,
            130: 217,
            535: 218,
            131: 219,
            132: 220,
            258: 221,
            133: 222,
            134: 223,
            299: 224,
            259: 225,
            537: 226,
            536: 227,
            544: 228,
            538: 229,
            540: 230,
            542: 231,
            135: 232,
            136: 233,
            532: 234,
            358: 235,
            137: 236,
            138: 237,
            139: 238,
            140: 239,
            311: 240,
            141: 241,
            142: 242,
            539: 243,
            541: 244,
            317: 245,
            143: 246,
            318: 247,
            144: 248,
            146: 249,
            147: 250,
            148: 251,
            149: 252,
            357: 253,
            150: 254,
            151: 255,
            633: 256,
            152: 257,
            153: 258,
            154: 259,
            155: 260,
            304: 261,
            302: 262,
            306: 263,
            659: 264,
            156: 265,
            157: 266,
            158: 267,
            159: 268,
            293: 269,
            292: 270,
            160: 271,
            161: 272,
            162: 273,
            590: 274,
            592: 275,
            163: 276,
            164: 277,
            531: 278,
            165: 279,
            567: 280,
            569: 281,
            166: 282,
            568: 283,
            570: 284,
            167: 285,
            168: 286,
            288: 287,
            169: 288,
            545: 289,
            170: 290,
            260: 291,
            586: 292,
            514: 293,
            296: 294,
            298: 295,
            171: 296,
            172: 297,
            173: 298,
            174: 299,
            607: 300,
            609: 301,
            591: 302,
            175: 303,
            628: 304,
            176: 305,
            177: 306,
            322: 307,
            179: 308,
            181: 309,
            324: 310,
            182: 311,
            325: 312,
            323: 313,
            327: 314,
            261: 315,
            183: 316,
            184: 317,
            180: 318,
            657: 319,
            95: 320,
            96: 321,
            97: 322,
            94: 323,
            98: 324,
            262: 325,
            185: 326,
            186: 327,
            187: 328,
            188: 329,
            263: 330,
            189: 331,
            575: 332,
            190: 333,
            650: 334,
            191: 335,
            313: 336,
            653: 337,
            280: 338,
            333: 339,
            193: 340,
            642: 341,
            284: 342,
            588: 343,
            589: 344,
            126: 345,
            127: 346,
            194: 347,
            655: 348,
            196: 349,
            197: 350,
            198: 351,
            648: 352,
            578: 353,
            577: 354,
            199: 355,
            200: 356,
            643: 357,
            266: 361,
            201: 362,
            329: 363,
            635: 364,
            331: 365,
            634: 358,
            328: 359,
            572: 366,
            289: 367,
            546: 368,
            587: 369,
            267: 370,
            291: 371,
            330: 360,
            290: 372,
            608: 373,
            202: 374,
            314: 375,
            654: 376,
            203: 377,
            204: 378,
            205: 379,
            206: 380,
            332: 381,
            207: 382,
            565: 383,
            208: 384,
            295: 385,
            210: 386,
            211: 387,
            212: 388,
            213: 389,
            214: 390,
            215: 391,
            216: 392,
            217: 394,
            341: 395,
            340: 393,
            218: 396,
            268: 397,
            219: 398,
            220: 399,
            221: 400,
            222: 401,
            223: 402,
            269: 403,
            300: 404,
            224: 405,
            326: 406,
            225: 407,
            270: 408,
            226: 409,
            626: 410,
            625: 411,
            227: 412,
            621: 413,
            348: 414,
            228: 415,
            316: 416,
            361: 417,
            658: 418,
            271: 419,
            229: 420,
            230: 421,
            231: 422,
            272: 423,
            232: 424,
            233: 425,
            234: 426,
            632: 427,
            275: 428,
            533: 429,
            235: 430,
            276: 431,
            277: 432,
            278: 433,
            279: 434,
            273: 435,
            274: 436,
            363: 437,
            364: 438,
            365: 439,
            366: 440,
            367: 441,
            581: 442,
            370: 443,
            559: 444,
            547: 445,
            371: 446,
            372: 447,
            373: 448,
            374: 449,
            601: 450,
            375: 451,
            376: 452,
            377: 453,
            378: 454,
            379: 455,
            380: 456,
            381: 457,
            382: 458,
            383: 459,
            384: 460,
            385: 461,
            386: 462,
            387: 463,
            388: 464,
            389: 465,
            390: 466,
            391: 467,
            552: 468,
            551: 469,
            553: 470,
            392: 471,
            595: 472,
            597: 473,
            393: 474,
            550: 475,
            394: 476,
            395: 477,
            396: 478,
            397: 479,
            398: 480,
            399: 481,
            400: 482,
            401: 483,
            402: 484,
            403: 485,
            404: 486,
            637: 487,
            405: 488,
            406: 489,
            407: 490,
            599: 491,
            408: 492,
            582: 493,
            409: 494,
            410: 495,
            411: 496,
            549: 497,
            548: 498,
            412: 499,
            413: 500,
            414: 501,
            415: 502,
            416: 503,
            417: 504,
            603: 505,
            418: 506,
            600: 507,
            419: 508,
            555: 509,
            554: 510,
            420: 511,
            580: 512,
            618: 513,
            421: 514,
            422: 515,
            423: 516,
            424: 517,
            560: 518,
            604: 519,
            606: 520,
            602: 521,
            425: 522,
            605: 523,
            426: 524,
            427: 525,
            428: 526,
            429: 527,
            430: 528,
            431: 529,
            432: 530,
            433: 531,
            434: 532,
            556: 533,
            435: 534,
            583: 535,
            557: 536,
            436: 537,
            437: 538,
            438: 539,
            439: 540,
            440: 541,
            441: 542,
            442: 543,
            444: 544,
            446: 545,
            447: 546,
            448: 547,
            449: 548,
            636: 549,
            450: 550,
            452: 551,
            579: 552,
            617: 553,
            455: 554,
            456: 555,
            457: 556,
            460: 557,
            458: 558,
            459: 559,
            461: 560,
            649: 561,
            585: 562,
            584: 563,
            619: 564,
            462: 565,
            463: 566,
            464: 567,
            465: 568,
            466: 569,
            467: 570,
            468: 571,
            469: 572,
            470: 573,
            471: 574,
            472: 575,
            473: 576,
            474: 577,
            475: 578,
            476: 579,
            477: 580,
            478: 581,
            479: 582,
            480: 583,
            481: 584,
            483: 585,
            485: 586,
            486: 587,
            487: 588,
            488: 589,
            489: 590,
            490: 591,
            491: 592,
            492: 593,
            558: 594,
            493: 595,
            494: 596,
            495: 597,
            496: 598,
            497: 599,
            498: 600,
            499: 601,
            500: 602,
            594: 603,
            596: 604,
            598: 605,
            501: 606,
            502: 607,
            503: 608,
            504: 609,
        }[id]
    except KeyError:
        return id


def eins_null(item):
    """returns true, false on given item"""
    if item == 1:
        val = "True"
    else:
        val = "NULL"
    return val


# +++++++++++ end functions +++++++++++


def get_addresses_from_old_db():
    """fetch addresses from old database and return items"""
    query = """SELECT DISTINCT ad.id+40000 as id,ad.name,ad.vorname,
                    ad.strasse,ad.ort,ad.plz,ad.land,ad.anrede_briefkopf,
                    ad.telefon,ad.telefon_mobil,ad.email,ad.id_fremd,
                    ad.name2,ad.vorname2
                FROM
                    t_address ad
                INNER JOIN
                    t_contracts co ON ad.id = co.id_mbd
                WHERE
                    co.id_rsv_status = 2
                    or co.id_rsv_status = 5
                    """
    return mysql(query, True)


def get_contracts_from_old_db():
    """fetch contracts from old database and return items"""
    query = """
            SELECT
                ad.id+40000 as id,
                ad.anrede_briefkopf,
                concat(co.akz_c, '/', co.akz_y ) as akz,
                co.besonderheit as 'sonstige_besonderheit',
                co.betnum as 'versicherungsnummer',
                co.date_ksm as 'datum_mandatsstaus',
                co.in_ram as 'in_ram',
                co.j_abtrg as 'abtretung',
                co.j_sftdl as 'jemals_sofortdarlehen',
                co.land as 'abschlussland',
                co.vp as 'versicherte_person',
                co.vp_dob as 'geburtsdatum_vp',
                co.vp_gender as 'geschlecht_vp',
                dl.bezeichnung as 'dienstleister',
                ge.idt_gesellschaft as 'id_versicherer',
                ge.vs_auswahl as 'versicherer_abschluss',
                km.id_ksm as 'id_mandatsstatus',
                km.bezeichnung as 'mandatsstatus',
                pr.bezeichnung as 'vorsortierung',
                co.vtrg_bt as 'vertrag_beendet',
                zb.bezeichnung as 'zusatzbaustein',
                rt.bezeichnung as 'ansprueche_geltend_gemacht_von',
                rw.bezeichnung as 'nachweis_rechtsnachfolge',
                va.bezeichnung as 'vertragsart',
                co.anlagedatum as 'created_at',
                aa.bezeichnung as 'anlageart',
                co.id_rsv_status as 'id_rsv_status'
            FROM
                t_address ad
            INNER JOIN t_contracts co ON
                ad.id = co.id_mbd
            LEFT JOIN t_dl dl ON
                co.dl = dl.id
            LEFT JOIN t_gesellschaft ge ON
                co.idt_gesellschaft = ge.idt_gesellschaft
            LEFT JOIN t_ksm km ON
                co.id_ksm = km.id_ksm
            LEFT JOIN t_presort pr ON
                co.idt_presort = pr.idt_presort
            LEFT JOIN t_rn_abtr rt ON
                co.id_rn_abtr = rt.id
            LEFT JOIN t_rn_nw rw ON
                co.id_rn_nw = rw.id
            LEFT JOIN t_vart va ON
                co.idt_vart = va.idt_vart
            LEFT JOIN t_zbs zb ON
                co.idt_zbs = zb.idt_zbs
            LEFT JOIN t_aart aa ON
                co.idt_aart = aa.idt_aart
            WHERE
                co.id_rsv_status = 2
                or co.id_rsv_status = 5
            ORDER BY
                ad.id
            """
    return mysql(query, True)


def get_address_ids_from_new_db():
    """fetch all address ids from new db and return items"""
    query = "SELECT id FROM t_address"
    return postgres(query, True)


def get_refnum_from_new_db():
    """fetch all refnums from new db and return items"""
    query = "SELECT refnum FROM t_contracts;"
    return postgres(query, True)


def insert_address_items():
    """compare old and new database address ids and returns none
    exisiting items of new database"""
    old_items = get_addresses_from_old_db()
    ids_old = [item["id"] for item in old_items]
    ids_new = [item[0] for item in sorted(get_address_ids_from_new_db())]
    ids_diff = sorted(set(ids_old).difference(ids_new))
    for item in old_items:
        if item["id"] in ids_diff:
            # check if it's a family and split fname
            if " und " in item["vorname"]:
                vorname1 = apo(item["vorname"].split(" und ")[0])
                vorname2 = apo(item["vorname"].split(" und ")[1])
                nachname1 = nachname2 = apo(item["name"])
            elif " u. " in item["vorname"]:
                vorname1 = apo(item["vorname"].split(" u. ")[0])
                vorname2 = apo(item["vorname"].split(" u. ")[1])
                nachname1 = nachname2 = apo(item["name"])
            elif " & " in item["vorname"]:
                vorname1 = apo(item["vorname"].split(" & ")[0])
                vorname2 = apo(item["vorname"].split(" & ")[1])
                nachname1 = nachname2 = apo(item["name"])
            else:
                vorname1 = apo(item["vorname"])
                nachname1 = apo(item["name"])
                vorname2 = nachname2 = "NULL"

            #  building the query
            query = """INSERT INTO public.t_address (
                id,lname,fname,street,city,zip,country,salutation,phone,
                mobile,mail,clienttype,customerid,lname2,fname2
            )
            VALUES
            ({},{},{},{},{},{},{},{},{},{},{},{},{},{},{});""".format(
                value_none(item["id"]),
                nachname1,
                vorname1,
                value_none(item["strasse"]),
                value_none(item["ort"]),
                value_none(item["plz"]),
                value_none(item["land"]),
                value_none(item["anrede_briefkopf"]),
                value_none(item["telefon"]),
                value_none(item["telefon_mobil"]),
                value_none(item["email"]),
                person(item["anrede_briefkopf"]),
                value_none(item["id_fremd"]),
                nachname2,
                vorname2
            )
            #  database query
            postgres(query)


def delete_contracts():
    """delete all contracts with an id < 50000"""
    query = "DELETE FROM t_contracts WHERE ida < 50000;"
    postgres(query)


def delete_addresses():
    """delete all addresses with an id < 50000"""
    query = "DELETE FROM t_address WHERE id < 50000;"
    postgres(query)


def new_contracts():
    """compare old and new database contract akz and returns none
    if there are exisiting items in new database"""
    old_items = get_contracts_from_old_db()
    new_items = []
    refnum_old = [item["akz"] for item in old_items]
    refnum_new = [item[0] for item in sorted(get_refnum_from_new_db())]
    refnum_diff = sorted(set(refnum_old).difference(refnum_new))
    for item in old_items:
        if item["akz"] in refnum_diff:
            new_items.append(item)

    return new_items


def insert_contract_items():
    """insert new refnum items in new database"""
    contracts = new_contracts()
    for contract in contracts:
        query = """INSERT INTO public.t_contracts (
                    id,ida,contnum,refnum,status,in_ram,created_at,created_by
                )
                VALUES
                ({},{},{},{},{},{},{},{});""".format(
            "uuid_generate_v4()",
            value_none(contract["id"]),
            value_none(contract["versicherungsnummer"]),
            value_none(contract["akz"]),
            value_none(contract["id_mandatsstatus"]),
            "'True'",
            value_none(contract["created_at"]),
            service_provider(contract["dienstleister"]),
        )

        postgres(query)

        # getting generated ids
        query = """SELECT id, refnum from t_contracts
                    WHERE refnum = '{}';""".format(
            contract["akz"]
        )
        new_id = postgres(query, True)

        # section 01 -> basisdaten -> t_cont_bd
        query = """UPDATE t_cont_bd SET "2b"={},"9a"={},
                        "last_update"={},last_user={}
                    WHERE id='{}';""".format(
            value_none(contract["versicherungsnummer"]),
            person(contract["anrede_briefkopf"]),
            "now()",
            "'gunther.weissenbaeck@kanzlei-mbd.local'",
            new_id[0][0],
        )
        postgres(query)

        # table t_cont_ms
        query = """UPDATE t_cont_ms SET id_ms={},dt_ms={},
                        "last_update"={},last_user={}
                    WHERE id='{}';""".format(
            mandatsstatus(contract["id_mandatsstatus"]),
            value_none(contract["datum_mandatsstaus"]),
            "now()",
            "'gunther.weissenbaeck@kanzlei-mbd.local'",
            new_id[0][0],
        )
        postgres(query)

        # table t_cont_rsv
        query = """UPDATE t_cont_rsv SET id_rsv={},
                        "last_update"={},last_user={}
                    WHERE id='{}';""".format(
            rsv(contract["id_rsv_status"]),
            "now()",
            "'gunther.weissenbaeck@kanzlei-mbd.local'",
            new_id[0][0],
        )
        postgres(query)

        # section 2 -> situationsabfrage -> t_cont_sa
        query = """UPDATE t_cont_sa SET "3a"={},"3b"={},"3c"={},
                        "3gh"={},"3j"={},"last_update"={},last_user={}
                    WHERE id='{}';""".format(
            ccc(contract["abschlussland"]),
            cont_type(contract["vorsortierung"], contract["vertragsart"]),
            cont_status(contract["vorsortierung"],
                        contract["vertrag_beendet"]),
            adding_block(contract["zusatzbaustein"]),
            eins_null(contract["abtretung"]),
            "now()",
            "'gunther.weissenbaeck@kanzlei-mbd.local'",
            new_id[0][0],
        )
        postgres(query)

        # section 3 -> versicherungsnehmer/versicherte person -> t_cont_sa
        query = """UPDATE t_cont_vn SET "11a"={},"11b"={},"11c"={},
                        "last_update"={},last_user={}
                    WHERE id='{}';""".format(
            insuranced_person(contract["versicherte_person"]),
            value_none(contract["geburtsdatum_vp"]),
            gender_insuranced_person(contract["geschlecht_vp"]),
            "now()",
            "'gunther.weissenbaeck@kanzlei-mbd.local'",
            new_id[0][0],
        )
        postgres(query)

        # section 4 -> unterlagen -> t_cont_ul
        query = """UPDATE t_cont_ul SET "13f"={},"last_update"={},last_user={}
                    WHERE id='{}';""".format(
            follow(contract["nachweis_rechtsnachfolge"]),
            "now()",
            "'gunther.weissenbaeck@kanzlei-mbd.local'",
            new_id[0][0],
        )
        postgres(query)

        # section 5 -> vertragsdaten -> t_cont_ul
        query = """UPDATE t_cont_vd SET "14a"={},"14b"={},"14q"={},"15"={},
                        "last_update"={},last_user={}
                    WHERE id='{}';""".format(
                        company(contract["id_versicherer"]),
                        value_none(contract["anlageart"]),
                        eins_null(contract["jemals_sofortdarlehen"]),
                        value_none(contract["sonstige_besonderheit"]),
                        "now()",
                        "'gunther.weissenbaeck@kanzlei-mbd.local'",
                        new_id[0][0],
        )
        postgres(query)


def address_worker():
    """a function to work them all in address table"""
    insert_address_items()


def contracts_worker():
    """a function to work them all in contracts table"""
    insert_contract_items()


def first_date_contracts():
    """fetch all contracts and its starting date"""
    query_old = "SELECT vnum, datum FROM t_zue WHERE ereignis IN (10)"
    old_entries = mysql(query_old, True)

    query_new = "SELECT contnum, id FROM t_contracts"
    new_entries = postgres(query_new, fetch=True)

    for item in old_entries:
        for i2 in new_entries:
            if i2[0] == item["vnum"]:
                query = """UPDATE t_cont_vd SET "14c"='{}'
                    WHERE id='{}';""".format(
                        item["datum"],
                        i2[1]
                    )
                postgres(query)


if __name__ == "__main__":
    print("delete addresses < 50000")
    delete_addresses()
    print("address worker")
    address_worker()

    print("delete all contracts ida < 50000")
    delete_contracts()
    print("contracts worker")
    contracts_worker()

    print("insert first date to contracts")
    first_date_contracts()

    print("finished")
