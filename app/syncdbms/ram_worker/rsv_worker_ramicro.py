"""module to sync the addresses between lvr app and ramicro"""
from time import time

from syncdbms.helpers import db_null, gender_greeting, salutation, ticks
from syncdbms.helpers.ticks import remove_ticks as rt
from syncdbms.logger import logging
from syncdbms.sql_worker.mssql import mssql
from syncdbms.sql_worker.postgres import postgres


def get_refnums_lvr():
    """query ids and refnums from lvr dbms"""
    sql = "SELECT id, refnum FROM t_contracts;"
    return postgres(sql, fetch=True)


def get_ram_rsv_per_contract():
    rows = get_refnums_lvr()
    res_ram_all = []
    for row in rows:
        sql = (
            f"SELECT '{row[0]}', ab.sBetreffZeile1 as RSV_SN, "
            f"ab.sBetreffZeile2 as RSV_VSN, "
            f"ab.sBetreffZeile3 as RSV_VN, "
            f"ad.sNachname as RSV, "
            f"ad.sBemerkung as RSV_BEMERKUNG, "
            f"ak.iAblagenummer as RSV_ABLAGENUMMER "
            f"FROM "
            f"RAMICRO.dbo.tblAkten ak "
            f"INNER JOIN RAMICRO.dbo.tblAktenBeteiligte ab ON "
            f"ak.GUIDAkte = ab.GUIDAkte "
            f"INNER JOIN RAMICRO.dbo.tblAdressen ad ON "
            f"ab.GUIDAdresse = ad.GUIDAdresse "
            f"WHERE ab.iBeteiligtenArt = 3 AND "
            f"ak.sAktenNummer = '{row[1]}';"
        )
        res_ram = mssql("RAMICRO", sql, fetch=True)
        if len(res_ram) != 0:
            res_ram_all += res_ram
    return res_ram_all


def insert_ram_cont_into_lvr():
    # empty the table
    sql = (
        f"UPDATE t_ram_rsv_cont SET rsv_sn=NULL,rsv_vsn=NULL,"
        f"rsv_vn=NULL,rsv=NULL,rsv_bemerkung=NULL,rsv_ablagenummer=NULL;"
    )
    postgres(sql)
    rows = get_ram_rsv_per_contract()
    for row in rows:
        sql = (
            f"UPDATE t_ram_rsv_cont SET "
            f"rsv_sn = '{rt(row[1])}', "
            f"rsv_vsn = '{rt(row[2])}', "
            f"rsv_vn='{rt(row[3])}', "
            f"rsv='{rt(row[4])}', "
            f"rsv_bemerkung='{rt(row[5])}', "
            f"rsv_ablagenummer='{row[6]}' "
            f"WHERE id='{row[0]}';"
        )
        # fill the table
        postgres(sql)


def null_ram_cont():
    sql = (
        "UPDATE t_ram_rsv_cont SET rsv_sn = NULL where rsv_sn='';"
        "UPDATE t_ram_rsv_cont SET rsv_vsn = NULL where rsv_vsn='';"
        "UPDATE t_ram_rsv_cont SET rsv_vn = NULL where rsv_vn='';"
        "UPDATE t_ram_rsv_cont SET rsv = NULL where rsv='';"
        "UPDATE t_ram_rsv_cont SET rsv_ablagenummer = NULL where "
        f"rsv_ablagenummer='0';"
    )
    postgres(sql)


def get_ids_lvr():
    """query address ids lvr dbms"""
    sql = "SELECT id FROM t_address;"
    return postgres(sql, fetch=True)


def get_ram_rsv_per_address():
    rows = get_ids_lvr()
    res_ram_all = []
    for row in rows:
        sql = (
            f"SELECT concat(ad2.sNachname, ' ', ad2.sAdresszusatz) as rsv,"
            f"ad1.iRechtsschutzAdresse as rsv_id,"
            f"ad1.sVersicherungsNummer as rsv_num, "
            f"ad1.sBemerkung as bemerkung, "
            f"ad1.iAdressNummer as id "
            f"FROM RAMICRO.dbo.tblAdressen ad1 "
            f"LEFT JOIN RAMICRO.dbo.tblAdressen ad2 ON "
            f"ad1.iRechtsschutzAdresse = ad2.iAdressNummer "
            f"WHERE ad1.iAdressNummer = {row[0]};")
        res_ram = mssql("RAMICRO", sql, fetch=True)
        if len(res_ram) != 0:
            res_ram_all += res_ram
    return res_ram_all


def insert_ram_addr_lvr():
    # empty the table
    sql = (
        f"UPDATE t_ram_rsv_addr SET rsv=NULL,rsv_id=NULL,rsv_num=NULL,"
        f"bemerkung=NULL;"
    )
    postgres(sql)

    # set the query
    rows = get_ram_rsv_per_address()
    for row in rows:
        sql = (
            f"UPDATE t_ram_rsv_addr SET rsv='{row[0]}',"
            f"rsv_id='{row[1]}',rsv_num='{row[2]}',bemerkung='{row[3]}' "
            f"WHERE id='{row[4]}';"
        )
        # fill the table
        postgres(sql)


def null_ram_addr():
    sql = (
        "UPDATE t_ram_rsv_addr SET rsv = NULL where rsv='' or rsv=' ';"
        "UPDATE t_ram_rsv_addr SET rsv_id = NULL where rsv_id='' OR rsv_id='None' or rsv_id='0';"
        "UPDATE t_ram_rsv_addr SET rsv_num = NULL where rsv_num='' OR rsv_num='None';"
        "UPDATE t_ram_rsv_addr SET bemerkung = NULL where bemerkung='' or bemerkung=' ';")
    postgres(sql)


def rsv_worker_ramicro():
    """a function to work them all"""
    time_begin = time()

    # do it per contract
    insert_ram_cont_into_lvr()
    null_ram_cont()

    # do it per address
    insert_ram_addr_lvr()
    null_ram_addr()

    time_end = time()
    logging.info(
        f"MSSQL: Done after {round(time_end - time_begin, 2)} seconds!")
