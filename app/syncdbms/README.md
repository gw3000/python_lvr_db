# Syncing Database Package

this package is to manage syncing databases between lv-r.de platform db and
the office internal lvr.kanzlei-mbd.local databases

## DBMS
These are the dbms the package works with:

- lv-r.de (api)
- lvr.kanzlei-mbd.local (postgres)
- ra micro (mssql)
- lvr old (mysql)

## Requirements
The following packages are required to run the syncdbms:

- mysql-connector
- psycopg2
- pyodbc
- python-env
- requests
- schedule

## Environment
To run the package create an ".env" which contains the following:

```
    # API LVR
    API_LVR_TOKEN=""

    # POSTGRES
    DB_PSQL_HOST=""
    DB_PSQL_USER=""
    DB_PSQL_PASSWD=""
    DB_PSQL_DB=""

    # MYSQL - OLD DATABASE
    DB_MYSQL_HOST=""
    DB_MYSQL_USER=""
    DB_MYSQL_PASSWD=""
    DB_MYSQL_DB=""
    DB_MYSQL_PORT=""

    # MSSQL
    DB_MSSQL_HOST=""
    DB_MSSQL_PORT=""
    DB_MSSQL_USER=""
    DB_MSSQL_PASSWD=""

    # SYNCING
    WRITE_BACK_DB="false"
    TIMEOUT_SYNC=1
```
