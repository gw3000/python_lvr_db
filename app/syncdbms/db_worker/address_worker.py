"""address worker to fetch address data from platform database"""
import json

from os import environ as e
from time import time

import requests

import dotenv

from syncdbms.logger import logging
from syncdbms.sql_worker import postgres

dotenv.load()

HEADER = {"Authorization": e.get("API_LVR_TOKEN")}
URL = "https://api.lv-r.de/clients?page="


def clear_import_adress_table():
    """clean up the import address table"""
    postgres("TRUNCATE TABLE t_import_address")
    logging.info("SQL: table t_import_address truncated")


def get_client_data():
    """fetches client address from platform database"""
    # fetch first page
    req = requests.get(URL + "1", headers=HEADER)
    if req.ok:  # if status is 200, do something
        data = json.loads(req.text)

        # fetch all continuing
        count = 2
        data_a = data
        while data != []:
            url_c = URL + str(count)
            req = requests.get(url_c, headers=HEADER)
            if req.ok:
                data = json.loads(req.text)
                data_a += data
            count += 1

        # init sql
        sql = ""
        for item in data_a:
            # don't import user muschol stuff
            if item["createdBy"]["user"]["id"] != 5:
                if item["typeOfClient"] != "spouses":
                    sql += """INSERT INTO t_import_address (id,lname,fname,
                    street,city,zip,country,"degree",salutation,phone,mobile,
                    mail,clienttype,customerid,created_at,inserted_at)
                    VALUES ('{}','{}','{}','{} {}','{}','{}','{}','{}','{}',
                    '{}','{}','{}','{}','{}','{}','{}');""".format(
                        item["id"],
                        item["lastname"],
                        item["firstname"],
                        item["streetAddress"].rstrip(" "),
                        item["streetNumber"],
                        item["city"],
                        item["zipcode"],
                        item["country"],
                        item["title"],
                        item["salutation"],
                        item["phonenumber"],
                        item["alternatePhonenumber"],
                        item["email"],
                        item["typeOfClient"],
                        item["createdBy"]["user"]["id"],
                        item["createdAt"],
                        "now()",
                    )
                else:
                    sql += """INSERT INTO t_import_address (id,lname,fname,
                   street,city,zip,country,"degree",salutation,phone,mobile,
                   mail,lname2,fname2,degree2,salutation2,mail2,clienttype,
                   customerid,created_at,inserted_at)
                   VALUES ('{}','{}','{}','{}''{}','{}','{}','{}','{}','{}',
                   '{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'
                   );""".format(
                        item["id"],
                        item["lastname"],
                        item["firstname"],
                        item["streetAddress"].rstrip(" "),
                        item["streetNumber"],
                        item["city"],
                        item["zipcode"],
                        item["country"],
                        item["title"],
                        item["salutation"],
                        item["phonenumber"],
                        item["alternatePhonenumber"],
                        item["email"],
                        item["relatedClient"]["lastname"],
                        item["relatedClient"]["firstname"],
                        item["relatedClient"]["title"],
                        item["relatedClient"]["salutation"],
                        item["relatedClient"]["email"],
                        item["typeOfClient"],
                        item["createdBy"]["user"]["id"],
                        item["createdAt"],
                        "now()",
                    )
        postgres(sql)
        logging.info("SQL: client address data imported to t_import_address")


def clean_up_import_table():
    """remove unsused stuff from import table"""
    sql = """UPDATE t_import_address SET degree = NULL
            WHERE degree = 'kein Titel';
        UPDATE t_import_address SET degree2 = NULL
            WHERE degree2 ='kein Titel';
        UPDATE t_import_address SET phone = NULL
            WHERE phone = 'None';
        UPDATE t_import_address SET mobile = NULL
            WHERE mobile = 'None';
        UPDATE t_import_address SET clienttype = 'eine Einzelperson'
            WHERE clienttype='single-person';
        UPDATE t_import_address SET clienttype = 'Eheleute/Lebenspartnerschaft'
            WHERE clienttype='spouses';
        UPDATE t_import_address SET
            clienttype = 'eine Einzelperson als Alleinerbe/Alleinerbin'
            WHERE clienttype='single-person-sole-inheritor';
        UPDATE t_import_address SET clienttype = 'eine Erbengemeinschaft'
            WHERE clienttype='joint-heirs';"""
    postgres(sql)
    logging.info("SQL: cleaned up addresses in t_import_address")


def insert_into_adress_table():
    """copies data from import table to address table"""
    postgres(
        """INSERT INTO t_address (SELECT aid.id, aid.lname, aid.fname,
        aid.street, aid.city, aid.zip, aid.country, aid."degree",
        aid.salutation, aid.phone, aid.mobile, aid.mail, aid.lname2,
        aid.fname2, aid.degree2, aid.salutation2, aid.mail2, aid.clienttype,
        aid.customerid, aid.in_ram, aid.created_at, aid.update_at
        FROM public.t_import_address aid
        LEFT JOIN t_address ad ON aid.id = ad.id WHERE ad.id IS null)"""
    )
    logging.info(
        """SQL: client addresses inserted from t_import_address to t_address"""
    )


def del_unused_adresses():
    """delete not used addresses from address table"""
    postgres(
        """DELETE FROM t_address WHERE id IN (SELECT ta.id FROM t_address ta
            LEFT JOIN t_contracts tc ON ta.id=tc.ida WHERE tc.ida IS NULL AND
            t_address.id>=50000)"""
    )
    logging.info("SQL: deleted unsused adresses")


def update_agent_addresses():
    """update addresses which were updated in platform"""
    postgres(
        """UPDATE
            t_agents
        SET
            lastname = ai.lastname,
            firstname = ai.firstname,
            company = ai.company,
            email = ai.email,
            streetaddress = ai.streetaddress,
            streetnumber = ai.streetnumber,
            zipcode = ai.zipcode,
            city = ai.city,
            country = ai.country,
            phonenumber = ai.phonenumber,
            alternatephonenumber = ai.alternatephonenumber,
            employeenumber = ai.employeenumber,
            "status" = ai."status",
            "role" = ai."role",
            ownerid = ai.ownerid
        FROM
            t_import_agents as ai
        WHERE
            t_agents.id = ai.id;"""
    )
    logging.info("SQL: updated addresses from platform to t_address")


def address_worker():
    """a function to work them all"""
    time_begin = time()
    clear_import_adress_table()
    get_client_data()
    clean_up_import_table()
    insert_into_adress_table()
    del_unused_adresses()
    update_agent_addresses()
    time_end = time()
    logging.info(f"SQL: Done after {round(time_end - time_begin, 2)} seconds!")


if __name__ == "__main__":
    address_worker()
