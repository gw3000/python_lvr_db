"""fetches and pushes agent data from the platform database"""
import json
from os import environ
from time import time

import dotenv
import requests
from syncdbms.logger import logging
from syncdbms.sql_worker import postgres

dotenv.load()

HEADER = {"Authorization": environ.get("API_LVR_TOKEN")}
URL = "https://api.lv-r.de/users?page="


def is_none(word):
    """if the read json value is none return NULL"""
    if word is None:
        return "NULL"
    return "'" + str(word) + "'"


def clear_import_agent_table():
    """empty the import agent table in the database"""
    postgres("TRUNCATE TABLE t_import_agents")
    logging.info("SQL: agents import table truncated")


def get_agent_data():
    """fetch agent data from the platform database"""
    # fetch first page
    req = requests.get(URL + "1", headers=HEADER)
    if req.ok:  # if status is 200, do something
        data = json.loads(req.text)
        # fetch all continuing
        count = 2
        data_a = data
        while data != []:
            url_c = URL + str(count)
            req = requests.get(url_c, headers=HEADER)
            if req.ok:
                data = json.loads(req.text)
                data_a += data
            count += 1

    # create sql query
    sql = ""
    for item in data_a:
        # aller vermittler außer muschol (id=5)
        if item["user"]["id"] != 5:
            user_id = item["user"]["id"]
            if item["owner"]:
                if item["owner"]["user"]:
                    if item["owner"]["user"]["id"]:
                        ownerid = is_none(item["owner"]["user"]["id"])
            else:
                ownerid = is_none(item["ownerId"])

            sql += (
                """INSERT INTO t_import_agents (id, lastname, firstname,
                company, email, streetaddress, streetnumber, zipcode, city,
                country, phonenumber, alternatephonenumber, employeenumber,
                status, created_at, inserted_at, role, ownerid) VALUES ({}, {},
                {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
                {});"""
            ).format(
                item["user"]["id"],
                is_none(item["lastname"]),
                is_none(item["firstname"]),
                is_none(item["company"]),
                is_none(item["user"]["email"]),
                is_none(item["streetAddress"]),
                is_none(item["streetNumber"]),
                is_none(item["zipcode"]),
                is_none(item["city"]),
                is_none(item["country"]),
                is_none(item["phonenumber"]),
                is_none(item["alternatePhonenumber"]),
                is_none(item["employeeNumber"]),
                is_none(item["status"]),
                is_none(item["user"]["createdAt"]),
                "now()",
                is_none(item["user"]["roles"][0]["name"]),
                ownerid,
            )
            ownerid = ""
    postgres(sql)
    logging.info("SQL: agent address data imported")


def insert_into_agents_table():
    """put data from import table to agent table"""
    sql = """INSERT INTO t_agents (SELECT aid.id, aid.lastname, aid.firstname,
        aid.company, aid.email, aid.streetaddress, aid.streetnumber,
        aid.zipcode, aid.city, aid.country, aid.phonenumber,
        aid.alternatephonenumber, aid.employeenumber, aid.status,
        aid.created_at, aid.role, aid.ownerid
        FROM public.t_import_agents aid LEFT JOIN t_agents ag ON aid.id = ag.id
    WHERE ag.id IS null);"""
    postgres(sql)
    logging.info("SQL: agents data inserted")


def update_agent_status():
    """update agent status in agent table"""
    sql = (
        "UPDATE	t_agents SET "
        + "status =(SELECT status FROM t_import_agents "
        + "WHERE t_agents.id = t_import_agents.id);"
    )
    postgres(sql)
    logging.info("SQL: agents status updated")


def update_agent_role():
    """update agent role in agent table"""
    sql = """UPDATE t_agents SET role =(SELECT role FROM
           t_import_agents WHERE t_agents.id = t_import_agents.id);"""
    postgres(sql)
    logging.info("SQL: agents role updated")


def update_agent_owner_id():
    """update agent id  in agent table"""
    sql = """UPDATE t_agents SET ownerid =(SELECT ownerid FROM
           t_import_agents WHERE t_agents.id = t_import_agents.id);"""
    postgres(sql)
    logging.info("SQL: agents ownerid updated")


def agent_worker():
    """a function to work them all"""
    time_begin = time()
    clear_import_agent_table()
    get_agent_data()
    insert_into_agents_table()
    update_agent_status()
    update_agent_role()
    update_agent_owner_id()
    time_end = time()
    logging.info(f"SQL: Done after {round(time_end - time_begin, 2)} seconds!")


if __name__ == "__main__":
    agent_worker()
