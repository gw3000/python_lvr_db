"""module to fetch contract data from platform database and puts it
into database"""

import json
from os import environ as e
from time import time

import dotenv
import requests

from syncdbms.logger import logging
from syncdbms.sql_worker import postgres

dotenv.load()


# api token
HEADER = {"Authorization": e.get("API_LVR_TOKEN")}

# all submitted contracts
URL_SUBM = "https://api.lv-r.de/contracts?status=Submitted"
# the specific contracts and its data
URL_SPEC = "https://api.lv-r.de/contracts/"  # + id


def clear_import_adress_table():
    """clean up the import address table in db"""
    postgres("TRUNCATE TABLE t_import_contracts")
    logging.info("SQL: table t_import_contracts truncated")


def get_contracts(url):
    """fetches the contract data from platform db"""
    req = requests.get(url + "&page=1", headers=HEADER)
    if req.ok:  # if status is 200, do something
        data = json.loads(req.text)
        # fetch all coninue
        counter = 2
        data_a = data
        while data != []:
            req = requests.get(url + "&page=" + str(counter), headers=HEADER)
            if req.ok:
                data = json.loads(req.text)
                data_a += data
            counter += 1
        sql = ""
        contid = []
        for item in data_a:
            if (item["client"] is not None and
                    item["createdBy"]["user"]["id"] != 5):
                contid.append(item["id"])
                sql += """INSERT INTO t_import_contracts (contnum,status,contid,
                    created_at,customerid,created_by,inserted_at)
                    VALUES
                    ('{}', '{}', '{}', '{}', '{}', '{}', '{}');""".format(
                    item["insuranceNumber"],
                    item["status"],
                    item["id"],
                    item["createdAt"],
                    item["client"]["id"],
                    item["createdBy"]["user"]["id"],
                    "now()",
                )
        if sql != "":
            postgres(sql)
            logging.info("SQL: contracts imported to t_import_contracts")
        else:
            logging.info("SQL: nothing to import to t_import_contracts")

        # contract data and refnum into t_import_contracts
        sql_cont = ""
        for item_cont in contid:
            req_cont = requests.get(URL_SPEC + item_cont, headers=HEADER)
            if req_cont.ok:  # if status is 200, do something
                data_cont = json.loads(req_cont.text)
                if data_cont["fileNumber"] is not None:
                    sql = """UPDATE t_import_contracts SET refnum = '{}',
                    data = '{}' WHERE contid = '{}';""".format(
                        data_cont["fileNumber"], data_cont["data"], item_cont
                    )
                    sql_cont += sql

        if sql_cont != "":
            postgres(sql_cont)
            logging.info(
                "SQL: contracts data and refnum imported to t_import_contracts"
            )
        else:
            logging.info("SQL: no contracts data or refnum imported")


def insert_into_contracts_table():
    """selects all unsean contracts from import table and puts it into
    contract table"""
    postgres(
        """INSERT INTO t_contracts(SELECT uuid_generate_v4(),
        ic.customerid, ic.contnum, ic.refnum, ic."data", ic.status, null,
        ic.created_at, ic.created_by
        FROM t_import_contracts ic
        LEFT JOIN t_contracts co
        ON ic.contnum=co.contnum
        WHERE co.contnum IS null AND ic.refnum IS NOT NULL)"""
    )
    logging.info("SQL: contracts data inserted into t_contracts")


def set_init_ms():
    """set initial m-status to new contract"""
    sql = (
        f"{'UPDATE t_cont_ms set id_ms=1001,last_update =now(),'}"
        f"{'dt_ms = now() WHERE id_ms IS NULL;'}"
        )
    postgres(sql)
    logging.info("SQL: set initial m-status to 1001")


def contracts_worker():
    """a function to work them all"""
    time_begin = time()
    clear_import_adress_table()
    get_contracts(URL_SUBM)
    insert_into_contracts_table()
    set_init_ms()
    time_end = time()
    logging.info(f"SQL: Done after {round(time_end - time_begin, 2)} seconds!")


if __name__ == "__main__":
    contracts_worker()
