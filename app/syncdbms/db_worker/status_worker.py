"""pulls and pushes status data from local db.
it returns a list of contract-id and ms helptext"""
from os import environ as e
from time import time

import dotenv
import requests

from syncdbms.logger import logging
from syncdbms.sql_worker import postgres

dotenv.load()


HEADER = {"Authorization": e.get("API_LVR_TOKEN")}


def get_ms_data():
    """pulls status data from db"""
    sql = """SELECT ic.contid, ms.helptext_pf FROM public.t_cont_ms cs
    INNER JOIN t_contracts co
    ON cs.id = co.id
    INNER JOIN t_import_contracts ic
    ON co.refnum = ic.refnum
    INNER JOIN t_ms ms
    ON cs.id_ms = ms.id
    WHERE cs.id_ms IS NOT NULL AND ms.helptext_pf IS NOT NULL"""
    items = postgres(sql, True)
    logging.info("MS-WORKER: fetched data from local database")
    return items


def push_ms_data(items):
    """pushes local ms data (contractid, helptext_pf)
    to remote platform db using the api."""

    for item in items:
        url = "https://api.lv-r.de/contracts/" + item[0]
        payload = {"clientStatus": item[1], "billing": ""}
        requests.patch(url, json=payload, headers=HEADER)

    # logging
    logging.info("MS-WORKER: wrote back mandatstatus")


def status_worker():
    """manages the ms data transfer between
    local and remote databases"""
    time_begin = time()
    if e.get("WRITE_BACK_DB") == "true":
        push_ms_data(get_ms_data())
        logging.info("Write back data to lv-r.de")
    else:
        logging.info("Did not write back anything to lv-r.de")
    time_end = time()
    logging.info(f"SQL: Done after {round(time_end - time_begin, 2)} seconds!")


if __name__ == "__main__":
    status_worker()
