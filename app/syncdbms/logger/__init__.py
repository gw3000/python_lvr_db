"""logger config module"""
import logging
import sys


# log config; write it to system stdout
logging.basicConfig(
    stream=sys.stdout,
    format="%(asctime)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)


def seperator(text=""):
    SEP = "-"[0]*100
    if text != "":
        print(SEP, "\n", " "*5, text)
    else:
        print(SEP)
