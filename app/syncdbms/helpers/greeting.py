def gender_greeting(greeting):
    """return value is a gender code and gender in letter greeting to the ram db"""
    if greeting == "Frau":
        grt = [2, "'Frau'"]
    elif greeting == "Herr":
        grt = [1, "'Herrn'"]
    elif greeting == "Herrn":
        grt = [1, "'Herrn'"]
    elif greeting == "Familie":
        grt = [9, "'Familie'"]
    elif greeting == "Eheleute":
        grt = [8, "'Eheleute'"]
    elif greeting == "Firma":
        grt = [4, "'Firma'"]
    else:
        grt = ["NULL", "NULL"]
    return grt


def gender_greeting_back(greeting):
    """return value is a simple gender string returning to postgres db"""
    if greeting.startswith("Her"):
        grt_bk = "'Herr'"
    elif greeting.startswith("Fra"):
        grt_bk = "'Frau'"
    elif greeting.startswith("Fam"):
        grt_bk = "'Familie'"
    elif greeting.startswith("Ehe"):
        grt_bk = "'Eheleute'"
    elif greeting.startswith("Fir"):
        grt_bk = "'Firma'"
    else:
        grt_bk = "NULL"
    return grt_bk