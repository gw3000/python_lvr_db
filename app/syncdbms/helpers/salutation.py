def salutation(greeting, name):
    """return salutation in depency of greeting"""
    if greeting == "Frau":
        sal = "'Sehr geehrte Frau {},'".format(name)
    elif greeting == "Herr":
        sal = "'Sehr geehrter Herr {},'".format(name)
    elif greeting == "Familie":
        sal = "'Sehr geehrte Familie {},'".format(name)
    else:
        sal = "NULL"
    return sal