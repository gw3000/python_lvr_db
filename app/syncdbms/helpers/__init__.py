"""helper module to translate wording stuff (greeting)"""
from syncdbms.helpers.db_null import db_null
from syncdbms.helpers.salutation import salutation
from syncdbms.helpers.greeting import gender_greeting, gender_greeting_back