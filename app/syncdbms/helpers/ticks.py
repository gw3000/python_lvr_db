def remove_ticks(val):
    """removes single ticks

    Args:
        val (string): varible that consists of a tick(s)

    Returns:
        [string]: returns a cleaned up string
    """
    if val.find("'"):
        return val.replace("'", "''")
