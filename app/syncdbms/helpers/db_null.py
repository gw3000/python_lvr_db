def db_null(value):
    """test if a result is None then the return value is NULL"""
    if value is None:
        dbn = "NULL"
    else:
        if isinstance(value, str):
            value = value.replace('\'', '')
            value = value.replace('\"', '')
        dbn = "'" + str(value).strip(' ') + "'"
    return dbn
