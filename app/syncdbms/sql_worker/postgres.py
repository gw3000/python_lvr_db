"""sql worker to help querying data from and to pssql dbms"""
from os import environ as e

import psycopg2

from syncdbms.logger import logging


def postgres(query, fetch=None):
    """sql worker to query db_lvr postgres database"""
    conn = None
    try:
        conn = psycopg2.connect(
            host=e.get("DB_PSQL_HOST"),
            database=e.get("DB_PSQL_DB"),
            user=e.get("DB_PSQL_USER"),
            password=e.get("DB_PSQL_PASSWD"),
        )
        cursor = conn.cursor()
        cursor.execute(query)
        if fetch:
            return cursor.fetchall()
        conn.commit()
        cursor.close()
    except (psycopg2.Error) as error:
        logging.error(error)
    finally:
        if conn is not None:
            conn.close()
