"""sql worker to help querying data from and to mysql dbms"""
from os import environ as e

import mysql.connector

from syncdbms.logger import logging


def sql_worker_mysql(query, fetch=None):
    """sql worker to query db_lvr mysql database"""
    conn = None
    try:
        conn = mysql.connector.connect(
            host=e.get("DB_MYSQL_HOST"),
            database=e.get("DB_MYSQL_DB"),
            user=e.get("DB_MYSQL_USER"),
            password=e.get("DB_MYSQL_PASSWD"),
            port=e.get("DB_MYSQL_PORT"),
        )
        cursor = conn.cursor(dictionary=True)
        cursor.execute(query)
        if fetch:
            return cursor.fetchall()
        conn.commit()
        cursor.close()
    except (mysql.connector.Error) as error:
        logging.error(error)
    finally:
        if conn is not None:
            conn.close()
