"""sql worker to help querying data from and to mssql dbms"""
from os import environ as e

import pyodbc

from syncdbms.logger import logging


def mssql(database, query, fetch=None):
    """sql worker to query ramicro, eloakte mssql database"""
    conn = None
    try:
        conn = pyodbc.connect(
            "DRIVER={ODBC Driver 17 for SQL Server};SERVER="
            + e.get("DB_MSSQL_HOST")
            + ";DATABASE="
            + database
            + ";UID="
            + e.get("DB_MSSQL_USER")
            + ";PWD="
            + e.get("DB_MSSQL_PASSWD")
            + ";Encrypt=no"
        )
        cursor = conn.cursor()
        cursor.execute(query)
        if fetch:
            return cursor.fetchall()
        conn.commit()
        cursor.close()
    except (pyodbc.Error) as error:
        logging.error(error)
        print(error)
    finally:
        if conn is not None:
            conn.close()
