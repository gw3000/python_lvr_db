# Todo LVR


## Dienstleister korrigieren
- Akte 55041/20 Obervermittler falsch

## Adressanlage und Rückspielen RAM
### Mandantkurzübersicht
- ~~Email-Adresse (9l) falsch~~
- ~~Mobil Telefon (9k) falsch~~

### Adressanlage
- Adressanlage ohne Status also immer, wenn neue Daten abgeholt werden.

### Aktenanlage
- Aktenanlage bei Click - Speichern der Akte in lvr
- Aktenanlage bei Status >= 1010

#### Aktenanlage bei Click
- sollte in einem separaten Docker-Container laufen

## Backup
- Backup lv-r.de (komplett Docker Stack)
