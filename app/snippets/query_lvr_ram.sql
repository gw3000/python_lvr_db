select
	co.ida
    concat(co.akz_c, '/' , akz_y) as sAktenNummer,
    concat(ad.name, ', ', ad.vorname, ' ./. ', ge.vs_darstellg) as abz,
    CURRENT_TIMESTAMP() as dtAnlage,
    '20' as iReferat,
    co.land as sAktenKennzeichen,
    '1' as aab,
    ge.vs_auswahl as bemerkung,
    '0' as gegenstandswert,
    '1899-12-30 00:00:00' as dtAblage,
    '0' as iAblageNummer,
    '1' as yAktenArt,
    (co.akz_y + 2000)as iAktenjahrgang,
    (ad.id + 40000) as iLaufendeNummer,
    '0' as yIDUnterakte,
    concat(ad.name, ', ', ad.vorname) as sMandant,
    'gw' as sDiktatzeichenErsteller,
    akz_c,
    akz_y,
    ad.id + 40000,
    ge.anum_ram,
    co.betnum
from
	t_contracts co
	inner join t_address ad on
	co.ida = ad.id
	inner join t_cont_vd tcv on
	co.id=tcv.id
	inner join t_isr ti on
	tcv."14a" = ti.id
	inner join t_gesellschaft ge on
	co.idt_gesellschaft = ge.idt_gesellschaft
where
	co.in_ram is null
	and id_mbd is not null
	and co.idt_gesellschaft > 0
	and ge.anum_ram is not null
	and ad.vorname is not null
order by
	id_mbd asc;

