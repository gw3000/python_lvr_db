"""module to test mssql database connection"""
from syncdbms.logger import logging, seperator
from syncdbms.sql_worker.mssql import mssql
from syncdbms.sql_worker.postgres import postgres
from syncdbms.sql_worker.mysql import sql_worker_mysql as mysql

import dotenv

dotenv.load()


def test_mssql():
    """testing the sql connection using fetches the version of
    the Microsoft SQL Database"""
    try:
        sql = "SELECT @@version;"
        row = mssql("master", sql, fetch=True)
        print(row[0][0])
        logging.info("SQL: MSSQL-Server succesfully fetched!")
    except Exception as error:
        print(error)


def test_postgres():
    """test postgres server"""
    try:
        sql = "SELECT version();"
        row = postgres(sql, fetch=True)
        for item in row:
            print(item[0])
            logging.info("SQL: POSTGRES-Server succesfully fetched!")
    except Exception as error:
        print(error)


def test_mysql():
    """test mysql server"""
    try:
        sql = "SELECT version();"
        row = mysql(sql, fetch=True)
        for item in row:
            print(f"MySQL-Server-Version: {item['version()']}")
            logging.info("SQL: MySQL-Server succesfully fetched!")
    except Exception as error:
        print(error)


if __name__ == "__main__":
    seperator()
    test_mssql()
    seperator()
    test_postgres()
    #  seperator()
    #  test_mysql()
    seperator()
