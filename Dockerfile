FROM python:3.8.0-buster

WORKDIR /app
ADD app /app

# installing the mssql driver
RUN apt-get update
RUN apt-get install -y apt-transport-https
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install msodbcsql17 unixodbc-dev -y

# update pip
RUN pip install --upgrade pip
# install python libs using pip
RUN pip install psycopg2 pyodbc requests python-env schedule mysql-connector

# change ssl settings
RUN sed -i 's#TLSv1.2#'"TLSv1.0"'#g' /etc/ssl/openssl.cnf
RUN sed -i 's#DEFAULT@SECLEVEL=2#'"DEFAULT@SECLEVEL=1"'#g' /etc/ssl/openssl.cnf

# running main script
CMD ["python", "main.py"]
